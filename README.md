EditMips
=====

EditMips es una interfaz grafica para desarrollar programas en lenguaje ensamblador MIPS. 

Inspirado en el desarrollo de [Rafael Ignacio Zurita](https://github.com/zrafa/mipsx)

```
  Copyright 2019 nomada-7g <gitnomada@gmail.com>

  You can redistribute it and/or modify it under the terms of the Apache License Version 2.0
  
  http://www.apache.org/licenses/ 
  
  Check LICENSE file.
  
 ```
