#!/bin/bash

source vars.sh

echo "Existing users : 'root/root' & 'user/user'"
if [ "$1" != "" ]; then
    USER="$1"
else
    USER="${USER}"
    echo "Credentials : '${USER}/${PASSWD}'"
fi
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p ${SSH_PORT} ${USER}@127.0.0.1 $*
exit 0
