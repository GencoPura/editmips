#!/bin/bash

source vars.sh
 
qemu-system-mips -M malta -m 512 \
                   -kernel ${KERNEL} \
                   -initrd ${INITRD} \
                   -hda ${HDD} \
                   -net nic \
                   -net user,hostfwd=tcp:127.0.0.1:${SSH_PORT}-:22,hostfwd=tcp:127.0.0.1:${EXTRA_PORT}-:${EXTRA_PORT} \
                   -display none -vga none -nographic \
                   -append 'nokaslr root=/dev/sda1 console=ttyS0'

exit 0
